import styled from "styled-components";
import {mobile} from "../components/responsive";
import {useRecoilState} from "recoil";
import {shoppingCartState} from "../components/recoilatoms";
import {useRouter} from 'next/router'

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  background: linear-gradient(rgba(255, 255, 255, 0.5),
  rgba(255, 255, 255, 0.5)),
  url("https://images.pexels.com/photos/6984650/pexels-photo-6984650.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940") center;
  background-size: cover;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Wrapper = styled.div`
  width: 25%;
  padding: 20px;
  background-color: white;
  ${mobile({width: "75%"})}
`;

const Title = styled.h1`
  font-size: 24px;
  font-weight: 300;
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

const Input = styled.input`
  flex: 1;
  min-width: 40%;
  margin: 10px 0;
  padding: 10px;
`;

const Button = styled.button`
  width: 40%;
  border: none;
  padding: 15px 20px;
  background-color: teal;
  color: white;
  cursor: pointer;
  margin-bottom: 10px;
`;

const Login = () => {

    const router = useRouter()

    const handleUserLogin = (e) => {
        e.preventDefault();
        if (cart.items.length === 0) {
            // Restore old cart
            try {
                fetch('/api/getLastCart', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({userId: cart.userId, api: "GET_LAST_CART"}),
                }).then(response => {
                    console.log("We here..")
                    return response.json();
                }).then(data => {
                    console.log("Response from GET_LAST_CART: ", data);
                    if(data.items && data.items.length > 0) {
                        setCart(oldState => ({
                            ...oldState,
                            items: data.items,
                            cartId: data.cartId,
                            workflowId: data.workflowId,
                            taskId: data.taskId,
                        }));
                    }
                    router.push('/')
                })
            } catch (e) {
            }
        } else {
            router.push('/')
        }
        return false;
    }

    const setUserId = (userId) => {
        setCart(oldState => ({
            ...oldState,
            userId: userId
        }));
    }
    const [cart, setCart] = useRecoilState(shoppingCartState);

    return (
        <Container>
            <Wrapper>
                <Title>SIGN IN</Title>
                <Form>
                    <Input placeholder="username" onChange={(e) => setUserId(e.target.value)} value={cart.userId}/>
                    <Input placeholder="password" type={"password"}/>
                    <Button onClick={(e) => handleUserLogin(e)}>LOGIN</Button>
                </Form>
            </Wrapper>
        </Container>
    );
};

export default Login;
