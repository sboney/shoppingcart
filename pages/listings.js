import Button from 'react-bootstrap/Button';
import 'bootstrap/dist/css/bootstrap.min.css';

export default function FirstPost() {
    return (
        <div>
            <h1>First Post</h1>
            <Button variant="primary">Primary</Button>
        </div>
    )
}
