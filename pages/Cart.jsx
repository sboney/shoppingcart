import {Add, Remove} from "@material-ui/icons";
import styled from "styled-components";
import Announcement from "../components/Announcement";
import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import {mobile} from "../components/responsive";
import {useRecoilState} from "recoil";
import {shoppingCartState} from "../components/recoilatoms";
import {popularProducts} from "../components/data";
import Link from "next/link";
import {useRouter} from 'next/router'

const Container = styled.div``;

const Wrapper = styled.div`
  padding: 20px;
  ${mobile({padding: "10px"})}
`;

const Title = styled.h1`
  font-weight: 300;
  text-align: center;
`;

const Top = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 20px;
`;

const TopButton = styled.button`
  padding: 10px;
  font-weight: 600;
  cursor: pointer;
  border: ${(props) => props.type === "filled" && "none"};
  background-color: ${(props) =>
          props.type === "filled" ? "black" : "transparent"};
  color: ${(props) => props.type === "filled" && "white"};
`;

const TopTexts = styled.div`
  ${mobile({display: "none"})}
`;
const TopText = styled.span`
  text-decoration: underline;
  cursor: pointer;
  margin: 0px 10px;
`;

const Bottom = styled.div`
  display: flex;
  justify-content: space-between;
  ${mobile({flexDirection: "column"})}

`;

const Info = styled.div`
  flex: 3;
`;

const Product = styled.div`
  display: flex;
  justify-content: space-between;
  ${mobile({flexDirection: "column"})}
`;

const ProductDetail = styled.div`
  flex: 2;
  display: flex;
`;

const Image = styled.img`
  width: 200px;
`;

const Details = styled.div`
  padding: 20px;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
`;

const ProductName = styled.span``;

const ProductId = styled.span``;

const ProductColor = styled.div`
  width: 20px;
  height: 20px;
  border-radius: 50%;
  background-color: ${(props) => props.color};
`;

const ProductSize = styled.span``;

const PriceDetail = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const ProductAmountContainer = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 20px;
`;

const ProductAmount = styled.div`
  font-size: 24px;
  margin: 5px;
  ${mobile({margin: "5px 15px"})}
`;

const ProductPrice = styled.div`
  font-size: 20px;
  font-weight: 200;
  ${mobile({marginBottom: "20px"})}
`;

const Hr = styled.hr`
  background-color: #eee;
  border: none;
  height: 1px;
`;

const Summary = styled.div`
  flex: 1;
  border: 0.5px solid lightgray;
  border-radius: 10px;
  padding: 20px;
  height: 50vh;
`;

const SummaryTitle = styled.h1`
  font-weight: 200;
`;

const SummaryItem = styled.div`
  margin: 30px 0px;
  display: flex;
  justify-content: space-between;
  font-weight: ${(props) => props.type === "total" && "500"};
  font-size: ${(props) => props.type === "total" && "24px"};
`;

const SummaryItemText = styled.span``;

const SummaryItemPrice = styled.span``;

const Button = styled.button`
  width: 100%;
  padding: 10px;
  background-color: black;
  color: white;
  font-weight: 600;
`;


const Cart = () => {
    const router = useRouter()
    const [cart, setCart] = useRecoilState(shoppingCartState);
    console.log("Card on load: ", cart)
    let productList = (<h1>Your cart is empty!</h1>)
    let subTotal = 0;
    let shipping = "0.00";
    if (cart.items && cart.items.length > 0) {
        const groupedItems = cart.items.reduce((total, value) => {
            total[value] = (total[value] || 0) + 1;
            return total;
        }, {});
        productList = Object.keys(groupedItems).map(function (itemId, itemCount) {
            console.log("Looking up: ", itemId)
            let product = popularProducts.filter(product => product.id === parseInt(itemId))[0];
            subTotal = subTotal + (product.price * groupedItems[itemId]);
            shipping = "9.90"
            return (
                <Product>
                    <ProductDetail>
                        <Image
                            src={product.img}/>
                        <Details>
                            <ProductName>
                                <b>Product:</b> {product.name}
                            </ProductName>
                            <ProductId>
                                <b>ID:</b> {product.id}
                            </ProductId>
                            <ProductColor color="black"/>
                            <ProductSize>
                                <b>Size:</b> NA
                            </ProductSize>
                        </Details>
                    </ProductDetail>
                    <PriceDetail>
                        <ProductAmountContainer>
                            <Add/>
                            <ProductAmount>{groupedItems[itemId]}</ProductAmount>
                            <Remove/>
                        </ProductAmountContainer>
                        <ProductPrice>Each: ${product.price}</ProductPrice>
                        <ProductPrice>Total: ${(product.price * groupedItems[itemId])}</ProductPrice>
                    </PriceDetail>
                </Product>
            );
        });
    }

    const handleCheckout = (e) => {
        e.preventDefault();
        console.log("Card on checkout : ", cart)
        if (cart.items.length > 0 && cart.userId !== "" && cart.workflowId !== "") {
            // Start checkout
            try {
                let postBody = {
                    ...cart,
                    api: "CHECKOUT",
                    amount: subTotal,
                };
                console.log("Checkout post: ", postBody)
                fetch('/api/cart', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(postBody),
                }).then(response => {
                    return response.json();
                }).then(data => {
                    console.log("Response from CHECKOUT: ", data);
                    setCart(oldState => ({
                        ...oldState,
                        items: [],
                        cartId: "",
                        workflowId: "",
                        taskId: "",
                    }));
                    router.push('/Checkout')
                })
            } catch (e) {
                console.error(e)
            }
        }
        return false;
    }


    return (
        <Container>
            <Announcement/>
            <Navbar/>
            <Wrapper>
                <Title>YOUR BAG</Title>
                <Top>
                    <Link href="/">
                        <TopButton>CONTINUE SHOPPING</TopButton>
                    </Link>
                    <TopTexts>
                        <TopText>Shopping Bag ({cart.items.length})</TopText>
                    </TopTexts>
                    <TopButton type="filled" onClick={(e) => handleCheckout(e)}>CHECKOUT NOW</TopButton>
                </Top>
                <Bottom>
                    <Info>
                        {productList}
                    </Info>
                    <Summary>
                        <SummaryTitle>ORDER SUMMARY</SummaryTitle>
                        <SummaryItem>
                            <SummaryItemText>Subtotal</SummaryItemText>
                            <SummaryItemPrice>$ {subTotal}.00</SummaryItemPrice>
                        </SummaryItem>
                        <SummaryItem>
                            <SummaryItemText>Estimated Shipping</SummaryItemText>
                            <SummaryItemPrice>$ {shipping}</SummaryItemPrice>
                        </SummaryItem>
                        <SummaryItem>
                            <SummaryItemText>Shipping Discount</SummaryItemText>
                            <SummaryItemPrice>$ -{shipping}</SummaryItemPrice>
                        </SummaryItem>
                        <SummaryItem type="total">
                            <SummaryItemText>Total</SummaryItemText>
                            <SummaryItemPrice>$ {subTotal}.00</SummaryItemPrice>
                        </SummaryItem>
                        <Link href="/Checkout">
                            <Button>CHECKOUT NOW</Button>
                        </Link>
                    </Summary>
                </Bottom>
            </Wrapper>
            <Footer/>
        </Container>
    );
};

export default Cart;
