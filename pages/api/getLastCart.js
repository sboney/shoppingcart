export default async function handler(req, res) {
    console.log(req.body)
    try {
        const lastCartReqRes = await fetch(getLastCartUrl(req.body.userId))
        const lastCartData = await lastCartReqRes.json()
        let result = lastCartData.results[0];
        if(result && result.workflowId) {
            let workflowId = result.workflowId;
            let workflow = await fetch(`https://conductorapp.trescommas.dev/api/workflow/${workflowId}?includeTasks=true`)
            let workflowData = await workflow.json()
            let updateCartTask = workflowData.tasks.filter(task => task.referenceTaskName === 'update_cart');
            if(Array.isArray(updateCartTask) && updateCartTask[0].status !== 'COMPLETED') {
                let outputData = updateCartTask[0].outputData;
                console.log("GET_LAST_CART Workflow update_cart outputData - ", outputData);
                if (outputData && outputData.items && Array.isArray(outputData.items)) {
                    res.status(200).json({
                        userId: outputData.userId,
                        cartId: workflowId,
                        workflowId: workflowId,
                        taskId: updateCartTask[0].taskId,
                        items: outputData.items
                    })
                    return;
                }
            }
        }
        res.status(200).json({})
    } catch (e) {
        console.error(e)
        res.status(200).json({"error": "true"})
    }
}


function getLastCartUrl(userId) {
    return 'https://conductorapp.trescommas.dev/api/workflow/search?' + new URLSearchParams({
        start: 0,
        size: 1,
        sort: "startTime",
        freeText: userId,
        query: "workflowType IN (order_checkout_flow) AND status IN (RUNNING) AND startTime > 1638168493313",
    });
}
