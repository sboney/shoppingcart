export default async function handler(req, res) {
    console.log("Request Detail: ", req.body.workflowId + " " + req.body.userId)
    try {
        if (req.body.workflowId && req.body.workflowId !== "") {
            let status = "IN_PROGRESS";
            if (req.body.api === 'CHECKOUT') {
                status = "COMPLETED"
            }
            let amount = 0;
            if (req.body.amount) {
                amount = req.body.amount;
            }
            let apiCall = await fetch("https://conductorapp.trescommas.dev/api/tasks", {
                body: JSON.stringify({
                    "status": status,
                    "taskId": req.body.taskId,
                    "workflowInstanceId": req.body.workflowId,
                    "outputData": {
                        "items": req.body.items,
                        "shipmentType": "USPS",
                        "amount": amount,
                        "userId": req.body.userId
                    }
                }),
                headers: {
                    "Content-Type": "application/json"
                },
                method: "POST"
            });
            let apiResponse = await apiCall.text();
            res.status(200).json({status: 'UPDATED', apiResponse: apiResponse})
            return;
        } else {
            try {
                if (req.body.userId && req.body.userId !== "") {
                    let apiCall = await fetch("https://conductorapp.trescommas.dev/api/workflow/order_checkout_flow", {
                        body: JSON.stringify({
                            "items": req.body.items,
                            "shipmentType": "USPS",
                            "amount": 0,
                            "userId": req.body.userId
                        }),
                        headers: {
                            "Content-Type": "application/json"
                        },
                        method: "POST"
                    });

                    let workflowId = await apiCall.text();

                    console.log("Workflow Id: ", workflowId);
                    let workflow = await fetch(`https://conductorapp.trescommas.dev/api/workflow/${workflowId}?includeTasks=true`)
                    let workflowData = await workflow.json()
                    let updateCartTask = workflowData.tasks.filter(task => task.referenceTaskName === 'update_cart');
                    console.log("updateCartTask", updateCartTask)
                    let postBody = {
                        "status": "IN_PROGRESS",
                        "taskId": updateCartTask[0].taskId,
                        "workflowInstanceId": updateCartTask[0].workflowInstanceId,
                        "outputData": {
                            "items": req.body.items,
                            "shipmentType": "USPS",
                            "amount": 0,
                            "userId": req.body.userId
                        }
                    };

                    console.log("postBody", postBody)
                    let apiCall2 = await fetch("https://conductorapp.trescommas.dev/api/tasks", {
                        body: JSON.stringify(postBody),
                        headers: {
                            "Content-Type": "application/json"
                        },
                        method: "POST"
                    });
                    let apiCall2Response = await apiCall2.text();

                    res.status(200).json({
                        status: 'CART_CREATED',
                        workflowId: updateCartTask[0].workflowInstanceId,
                        taskId: updateCartTask[0].taskId,
                        cartId: updateCartTask[0].workflowInstanceId,
                    });
                } else {
                    res.status(200).json({
                        status: 'NO_USER_ID_PRESENT'
                    });
                }
            } catch (e) {
                console.log(e);
                res.status(200).json({
                    status: 'CART_FAILED'
                });
                return;
            }
        }
    } catch (e) {
        console.log(e)
        res.status(200).json({
            status: 'OPERATION_FAILED'
        });
    }
}
