import {
    FavoriteBorderOutlined,
    SearchOutlined,
    ShoppingCartOutlined,
} from "@material-ui/icons";
import styled from "styled-components";
import {useRecoilCallback, useRecoilState} from "recoil";
import {shoppingCartState} from "./recoilatoms";

const Info = styled.div`
  opacity: 0;
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  background-color: rgba(0, 0, 0, 0.2);
  z-index: 3;
  display: flex;
  align-items: center;
  justify-content: center;
  transition: all 0.5s ease;
  cursor: pointer;
`;

const Container = styled.div`
  flex: 1;
  margin: 5px;
  min-width: 280px;
  height: 350px;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #f5fbfd;
  position: relative;

  &:hover ${Info} {
    opacity: 1;
  }
`;

const Circle = styled.div`
  width: 200px;
  height: 200px;
  border-radius: 50%;
  background-color: white;
  position: absolute;
`;

const Image = styled.img`
  height: 75%;
  z-index: 2;
`;

const Icon = styled.div`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  background-color: white;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 10px;
  transition: all 0.5s ease;

  &:hover {
    background-color: #e9f5f5;
    transform: scale(1.1);
  }
`;

let requestInProgress = false;

const Product = ({item}) => {
    const [cart, setCart] = useRecoilState(shoppingCartState);
    let shoppingCart = cart;
    const addItemFunction = (itemId) => {
        shoppingCart = {
            ...cart,
            "items" : [...cart.items]
        };
        shoppingCart.items.push(itemId);
        try {
            fetch('/api/cart', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({...shoppingCart, api: "UPDATE_CART"}),
            }).then(response => response.json())
                .then(data => {
                    console.log("Response from adding product: ", data);
                    if(data.status === 'CART_CREATED') {
                        setCart({
                            ...shoppingCart,
                            workflowId: data.workflowId,
                            taskId: data.taskId,
                            cartId: data.cartId
                        });
                    } else {
                        setCart({
                            ...shoppingCart,
                        });
                    }
                })
        } catch (e) {
        }
    };

    const logCartItems = useRecoilCallback(({snapshot}) => async () => {
        const shoppingCart = await snapshot.getPromise(shoppingCartState);
        console.log('Items in cart: ', shoppingCart);
    }, []);

    return (
        <Container>
            <Circle/>
            <Image src={item.img}/>
            <Info>
                <Icon>
                    <ShoppingCartOutlined onClick={() => addItemFunction(item.id)}/>
                </Icon>
                <Icon>
                    <SearchOutlined/>
                </Icon>
                <Icon>
                    <FavoriteBorderOutlined/>
                </Icon>
            </Info>
        </Container>
    );
};

export default Product;
