import Announcement from "./Announcement";
import Navbar from "./Navbar";
import Products from "./Products";
import Newsletter from "./Newsletter";
import Footer from "./Footer";
import React from "react";


const ShoppingCart = ({}) => {
    return (
        <div>
            <Announcement/>
            <Navbar/>
            <Products/>
            <Newsletter/>
            <Footer/>
        </div>
    );
};

export default ShoppingCart;
