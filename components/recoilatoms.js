import {atom} from "recoil";

export const shoppingCartState = atom({
    key: 'shoppingCartState', // unique ID (with respect to other atoms/selectors)
    default: {
        "userId": "",
        "cartId": "",
        "workflowId": "",
        "taskId": "",
        "items": []
    }, // default value (aka initial value)
});

